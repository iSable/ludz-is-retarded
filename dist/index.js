"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const discord_js_commando_1 = require("discord.js-commando");
const path = require("path");
const Reference_1 = require("./Reference");
const client = new discord_js_commando_1.CommandoClient({
    commandPrefix: Reference_1.default.prefix,
    owner: Reference_1.default.owner,
    disableEveryone: true,
    unknownCommandResponse: false
});
client.on("ready", () => {
    console.log("yes");
});
client.registry.registerDefaultTypes();
client.registry.registerCommandsIn(path.join(__dirname, "commands"));
client.registry.registerDefaultCommands({
    commandState: false,
    eval_: false,
    help: false,
    ping: false,
    prefix: false
});
const shutDown = () => {
    console.warn(`${client.user.username} was ordered to shweep`);
    client.destroy();
};
process.on("SIGINT", () => shutDown()).on("SIGTERM", () => shutDown());
client.login(Reference_1.default.token);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi9zcmMvaW5kZXgudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSw2REFBcUQ7QUFDckQsNkJBQTZCO0FBQzdCLDJDQUFvQztBQUVwQyxNQUFNLE1BQU0sR0FBRyxJQUFJLG9DQUFjLENBQUM7SUFDaEMsYUFBYSxFQUFFLG1CQUFTLENBQUMsTUFBTTtJQUMvQixLQUFLLEVBQUUsbUJBQVMsQ0FBQyxLQUFLO0lBQ3RCLGVBQWUsRUFBRSxJQUFJO0lBQ3JCLHNCQUFzQixFQUFFLEtBQUs7Q0FDOUIsQ0FBQyxDQUFDO0FBRUgsTUFBTSxDQUFDLEVBQUUsQ0FBQyxPQUFPLEVBQUUsR0FBRyxFQUFFO0lBQ3RCLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7QUFDckIsQ0FBQyxDQUFDLENBQUM7QUFFSCxNQUFNLENBQUMsUUFBUSxDQUFDLG9CQUFvQixFQUFFLENBQUM7QUFDdkMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxVQUFVLENBQUMsQ0FBQyxDQUFDO0FBQ3JFLE1BQU0sQ0FBQyxRQUFRLENBQUMsdUJBQXVCLENBQUM7SUFDdEMsWUFBWSxFQUFFLEtBQUs7SUFDbkIsS0FBSyxFQUFFLEtBQUs7SUFDWixJQUFJLEVBQUUsS0FBSztJQUNYLElBQUksRUFBRSxLQUFLO0lBQ1gsTUFBTSxFQUFFLEtBQUs7Q0FDZCxDQUFDLENBQUM7QUFFSCxNQUFNLFFBQVEsR0FBRyxHQUFHLEVBQUU7SUFDcEIsT0FBTyxDQUFDLElBQUksQ0FBQyxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBUSx3QkFBd0IsQ0FBQyxDQUFDO0lBQzlELE1BQU0sQ0FBQyxPQUFPLEVBQUUsQ0FBQztBQUNuQixDQUFDLENBQUM7QUFFRixPQUFPLENBQUMsRUFBRSxDQUFDLFFBQVEsRUFBRSxHQUFHLEVBQUUsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxTQUFTLEVBQUUsR0FBRyxFQUFFLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQztBQUN2RSxNQUFNLENBQUMsS0FBSyxDQUFDLG1CQUFTLENBQUMsS0FBSyxDQUFDLENBQUMifQ==