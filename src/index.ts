import { CommandoClient } from "discord.js-commando";
import * as path from "path";
import Reference from "./Reference";

const client = new CommandoClient({
  commandPrefix: Reference.prefix,
  owner: Reference.owner,
  disableEveryone: true,
  unknownCommandResponse: false
});

client.on("ready", () => {
  console.log("yes");
});

client.registry.registerDefaultTypes();
client.registry.registerCommandsIn(path.join(__dirname, "commands"));
client.registry.registerDefaultCommands({
  commandState: false,
  eval_: false,
  help: false,
  ping: false,
  prefix: false
});

const shutDown = () => {
  console.warn(`${client.user.username} was ordered to shweep`);
  client.destroy();
};

process.on("SIGINT", () => shutDown()).on("SIGTERM", () => shutDown());
client.login(Reference.token);
